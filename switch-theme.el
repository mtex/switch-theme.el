;;; switch-theme.el --- Like load-theme but disabling previously loaded themes -*- lexical-binding: t; -*-

;; Author: Manolito
;; Maintainer: Manolito
;; Created: 11 Sep 2022
;; Modified: 11 Sep 2022
;; Version: 0.1.0
;; Package-Requires: ((emacs "22.1"))
;; Keywords: theme
;; URL: https://codeberg.org/mtex/switch-theme.el

;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 3, or (at
;; your option) any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;;; Commentary:

;; `switch-theme' use `load-theme' but it also disable previously
;; loaded themes in the same operation.  Please use `load-theme' if
;; you want to load various themes at the same time.

;;; Code:

(defun switch-theme ()
  "Switch interactively theme.
Does the same as `load-theme' but it also run `disable-theme' for
each previously loaded theme."
  (interactive)
  (let ((current-themes custom-enabled-themes))
    (call-interactively #'load-theme)
    (mapc #'disable-theme current-themes)))

(provide 'switch-theme)

;;; switch-theme.el ends here
